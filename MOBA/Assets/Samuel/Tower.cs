﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public float sphereRadius;
    public float maxDistance;

    public LayerMask layerCollision;

    private Vector3 origin;
    private Vector3 direction;

    private float currentHitDistance;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            sphereRadius = 10;
        }
        else if (Input.GetKey(KeyCode.Z))
        {
            sphereRadius = 5;
        }
        
        origin = transform.position;
        direction = Vector3.forward;
        RaycastHit[] hits = Physics.SphereCastAll(origin, sphereRadius, direction, maxDistance, layerCollision);
        foreach (var hit in hits)
        {
            if (hit.transform.CompareTag("cube"))
            {
                hit.transform.GetComponent<Renderer>().material.color = Color.red;
            } 
            else if (hit.transform.CompareTag("sphere"))
            {
                hit.transform.GetComponent<Renderer>().material.color = Color.yellow;
            }
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color=Color.blue;
        //Debug.DrawLine(origin,origin+direction*currentHitDistance);
        Gizmos.DrawWireSphere(origin+direction*currentHitDistance,sphereRadius);
    }
}
